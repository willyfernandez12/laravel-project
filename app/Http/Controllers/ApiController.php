<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\State;

class ApiController extends Controller
{
    public function getCountry(){

        $paises = Country::get()->toJson(JSON_PRETTY_PRINT);
        return response($paises, 200);
    }

    public function findStates($id){

        $states = Country::find($id)->states->toJson(JSON_PRETTY_PRINT);
        return response($states, 200);
    }
}
