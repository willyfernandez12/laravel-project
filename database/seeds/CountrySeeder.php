<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Country;

class CountrySeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->delete();
        $DataCountries = ['Country 1', 'Country 2'];
        foreach($DataCountries as $key => $value){
            $Countries = new Country();
            $Countries->name = $value;
            $Countries->save();
        }
        
    }
}


